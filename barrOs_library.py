import subprocess as sp
import networkx as nx
import matplotlib as mplib
# mplib.use("Agg")
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd

import threading
import itertools
import os
import sys
import pickle

import warnings
warnings.filterwarnings("ignore")

from numpy import *
from math import *
from random import *
from scipy import stats

## DEFINE DEFAULT PARAMETERS
accepted_input_types = ['pdb_file', 'pdbID', 'pdb_folder', 'hhpred', 'pdbsum_fasta']
accepted_modes = ['membrane', 'non-membrane', 'all']

## 0. FOR LOGGING

## 0.1 Define automatic logger

class Logger(object):
    def __init__(self, name):
        self.terminal = sys.stdout
        self.log = open(name, "a")

    def write(self, message):
        self.terminal.write(message)
        self.log.write(message)  

    def flush(self):
        #this flush method is needed for python 3 compatibility.
        #this handles the flush command by doing nothing.
        #you might want to specify some extra behavior here.
        pass    


## 1. FUNCTIONS FOR INPUT CONTROL

## 1.1. Printing funtions

def print_hello():

    print("\n-------------------------------------------------------------------")
    print("\n                     WELCOME TO BARRoS   v0                      ")
    print("               Let me find all barrels in your PDBs                ")                                        
    print("                                                                   ")
    print("             Last change: 21.03.2024   Joana Pereira               \n")
    print("   Created at: MPI for Developmental Biology (Protein Evolution)   ") 
    print("   Maintained at: Biozentrum (Protein Structural Bioinformatics)   \n")    
    print("-------------------------------------------------------------------\n")
        
def print_help():

    print("\nUsage: python3 barrOs.py -in:<type>:<input_file> -out:<mode>\n")
    print("Parameters:")
    print("     -in:<type>         \tan input string (or list of strings) with one of the following types: {}".format(accepted_input_types))
    print("                        \tfor example: -in:pdbID:1LML_A,3XML_A or -in:pdb_file:1LML_A.pdb,3XML_A.pdb")
    print("                        \tATENTION: barrOs only works with monomeric proteins, so always provide the target chain")
    print("     -mode:             \tdefines the mode of running, i.e. either we want to deal only with membrane proteins, all, or non-membrane proteins")
    print("                        \tvalues accepted: {}".format(accepted_modes))   
    print("     -outputf:          \tdefines the name of the output file")
    print("                        \tit is not mandatory. Default: BARRoS_results.csv")  
    print("     -nodelete:         \tflag to define if files downloaded but without ") 
    print("                        \tdetected barrels should not be deleted. Default: False") 
    print("     -strandist:        \tmaximum distance between strands for a contact")
    print("                        \tto be considered. Default: 5 Ang") 
    print("     -extract_hairpins: \tflag to extract beta-hairpins, always breaking on the shortest loop. Default: False")


def print_summary(input_files, input_types, input_mode):

    print("           Input: {}".format(input_files))
    print("     Input types: {}".format(input_types))
    print("            Mode: {}\n".format(input_mode))
    
## 1.2. Functions to parse the inputs from the command line

def get_inputs(argv):

    found_input = False
    found_mode = False
    complexes_only = False
    multimer_only = False
    delete = True
    extract_hairpins = False
    input_files = []
    input_types = [] 
    input_type = 'nan'
    input_mode = 'nan'
    distance_threshold = 5
    output_file = 'BARRoS_results.csv'

    if len(argv) > 1:
        for arg in argv:
            if '-help' in arg:
                print_help()
                input_mode = 'help'
                exit            
            elif '-in' in arg:
                found_input = True
                found_mode = False
                input_type = arg.split(':')[1]
                input_file = arg.split(':')[2]            
            elif '-mode' in arg:
                found_mode = True
                found_input = False
                tmp_mode = arg.split(':')[1]
            elif '-outputf' in arg:
                output_file = arg.split(':')[1]
            elif '-complexes' in arg:
                complexes_only = True
            elif '-multimer' in arg:
                multimer_only = True
            elif '-nodelete' in arg:
                delete = False
            elif '-extract_hairpins' in arg:
                extract_hairpins = True
            elif '-strandist' in arg:
                distance_threshold = float(arg.split(':')[1])

            if found_input and not found_mode:
                if input_type in accepted_input_types:
                    for inpt in input_file.split(','):
                        input_files.append(inpt)
                        input_types.append(input_type)
                else:
                    print("ERROR: input type '{}' not accepted. Will ignore it.\n".format(input_type))
            elif not found_input and found_mode:
                input_mode = tmp_mode
            
    return input_files, input_types, input_mode, output_file, complexes_only, multimer_only, delete, distance_threshold, extract_hairpins     

## 1.3. Functions to check if the inputs are correct

def input_check(input_files, input_mode):

    if len(input_files) == 0 or input_mode not in accepted_modes :
        if len(input_files) == 0:
            print("ERROR: There are no input files.")
        if input_mode not in accepted_modes:
            if input_mode == 'nan':
                print("ERROR: Input mode not provided.")
            else:
                print("ERROR: '{}' mode possible.".format(input_mode))

        return 'incorrect'
    else:
        return 'all good'

## 2. FUNCTIONS FOR INPUT PROCESSING

## 2.1 Functions to parse non-pdb files

def parse_hhpred_output(hhpred_file, min_len = 25):

    print(" ... Parsing HHpred file '{}'".format(hhpred_file))

    pdbs = []
    found_hit_table = False
    
    with open(hhpred_file, 'r') as inhhpred:
        for line in inhhpred:
            if 'No' in line and 'Hit' in line and 'Prob' in line and 'E-value' in line and '=' not in line:
                found_hit_table = True
            elif found_hit_table:
                if len(line) > 10:
                    line_data = line.split()
                    if int(line_data[-2].split('-')[-1]) - int(line_data[-2].split('-')[0]) > min_len:
                        pdbs.append(line_data[1])
                    else:
                        found_hit_table = False
                else:
                    found_hit_table = False
    return pdbs

def parse_pdbsum_fasta(pdbsum_fasta, min_length = 30):

    print(" ... Parsing PDBsum file '{}'".format(pdbsum_fasta))
    print(" ... ...  May take some time!")
    
    pdbs = []
    sequences = []

    with open(pdbsum_fasta, 'r') as inpdbsum:
        for line in inpdbsum:
            if line.startswith('>'):
                pdb = line.strip()[1:]
                if pdb[-1] == ':':
                    pdb += 'A'
                pdbID = '{}_{}'.format(pdb.split(':')[0].upper(), pdb.split(':')[-1])
            else:
                seq = line.strip()
                if seq not in sequences and len(seq) >= min_length:
                    sequences.append(seq)
                    pdbs.append(pdbID)
    return pdbs

def parse_DSSPout(dsspout_file):

    found_table = False
    data_dict = {'ResNum':[], 'AA': [], 'SecStr': [], 'Phi': [], 'Psi': [], 'Chains': []}
    
    with open(dsspout_file, "r") as dsspout:
        for line in dsspout:
            if "#  RESIDUE AA STRUCTURE BP1" in line:
                found_table = True
            elif found_table:
                resnum = line[5:10].strip()
                secstr = line[16]
                secstr_compl = line[23]
                
                if secstr in [' ', 'S', 'B', 'T']:
                    secstr = 'L'
                elif secstr in ['G', 'I']:
                    secstr = 'H'

                if secstr == 'L':
                    secstr = '-'
                    
                res = line[13]
                phi = line[103:109].strip()
                psi = line[110:116].strip()
                chain = line[10:13].strip()

                if res != '!':
                    data_dict['ResNum'].append(resnum)
                    data_dict['AA'].append(res)
                    data_dict['SecStr'].append(secstr)
                    data_dict['Phi'].append(phi)
                    data_dict['Psi'].append(psi)
                    data_dict['Chains'].append(chain)

    for i in range(len(data_dict['SecStr'])):
        if i > 0 and i < len(data_dict['SecStr'])-1:
            if data_dict['SecStr'][i] == '-':
                if data_dict['SecStr'][i-1] != '-' and data_dict['SecStr'][i+1] != '-':
                    if data_dict['SecStr'][i-1] == data_dict['SecStr'][i+1]:
                        data_dict['SecStr'][i] = data_dict['SecStr'][i-1] 
    return data_dict

## 2.2. Functions to generate pdb lists and download pdb files
    
def download_pdb(pdbID, pdbs_folder = 'downloaded_pdbs/'):

    # create output folder in case it does not exist
    if not os.path.exists(pdbs_folder):

        try:
            make_outfolder = sp.Popen(['mkdir', pdbs_folder], stdout=sp.PIPE, stderr=sp.PIPE, stdin=sp.PIPE)
            stdout, stderr = make_outfolder.communicate()
        except:
            os.system('mkdir {}'.format(pdbs_folder))

    pdb = pdbID.split('_')[0]
    chain = pdbID.split('_')[1]
    protein_type = ''
    
    downloaded_pdb = '{}pdb{}.ent'.format(pdbs_folder, pdb.lower())

    # try the OMP database first
    #print(" ... Trying to download {} from the OMP database".format(pdb))   

    if not os.path.isfile(downloaded_pdb):
        try:         
            download_OMP = sp.Popen(['wget', 'https://opm-assets.storage.googleapis.com/pdb/{}.pdb'.format(pdb.lower(), pdb.lower())], stdout=sp.PIPE, stderr=sp.PIPE, stdin=sp.PIPE)
            stdout, stderr = download_OMP.communicate()
        except:
            os.system('wget https://opm-assets.storage.googleapis.com/pdb/{}.pdb'.format(pdb.lower(), pdb.lower()))

        if os.path.isfile('{}.pdb'.format(pdb.lower())):
            os.system('cp {}.pdb {}'.format(pdb.lower(), downloaded_pdb))
            os.system('rm {}.pdb'.format(pdb.lower()))
            protein_type = 'membrane'
            membrane_thickness = parse_membrane_thickness_from_OMP(downloaded_pdb)
        
        else:
            protein_type = 'non-membrane'
            membrane_thickness = 'nan'
            
            # try then the PDB_REDO 
            #print(" ... Trying to download {} from the PDB_REDO".format(pdb))
            try:
                download_pdbredo = sp.Popen(['wget', 'https://pdb-redo.eu/db/{}/{}_final.pdb'.format(pdb.lower(), pdb.lower())], stdout=sp.PIPE, stderr=sp.PIPE, stdin=sp.PIPE)
                stdout, stderr = download_pdbredo.communicate()
            except:
                os.system('wget https://pdb-redo.eu/db/{}/{}_final.pdb'.format(pdb.lower(), pdb.lower()))
            
            if os.path.isfile('{}_final.pdb'.format(pdb.lower())):
                os.system('cp {}_final.pdb {}'.format(pdb.lower(), downloaded_pdb))
                os.system('rm {}_final.pdb'.format(pdb.lower()))
                
            else: # if it does not work, download from PDBe
                #print(" ... Trying to download {} from the PDBe".format(pdb))
                try:
                    download_pdbe = sp.Popen(['wget', 'https://www.ebi.ac.uk/pdbe/entry-files/pdb{}.ent'.format(pdb.lower(), pdb.lower())], stdout=sp.PIPE, stderr=sp.PIPE, stdin=sp.PIPE)
                    stdout, stderr = download_pdbe.communicate()
                except:
                    os.system('wget https://www.ebi.ac.uk/pdbe/entry-files/pdb{}.ent'.format(pdb.lower(), pdb.lower()))
                
                if os.path.isfile('pdb{}.ent'.format(pdb.lower())):
                    os.system('cp pdb{}.ent {}'.format(pdb.lower(), downloaded_pdb))
                    os.system('rm pdb{}.ent'.format(pdb.lower()))
                else:
                    print(" ... Did not manage to download {} from OMP, PDB_REDO and PDBe".format(pdb))
                    return 'not available', 'not available', 'not available'
    
    else:
        print(' ---> Already downloaded {}. Just reading it'.format(downloaded_pdb))

    # Extract the target chain if the PDB corresponds to a full-atom model
    if os.path.isfile(downloaded_pdb):
        if not is_CA_only(downloaded_pdb):   

            protein_type = 'membrane'
            membrane_thickness = parse_membrane_thickness_from_OMP(downloaded_pdb)

            if membrane_thickness == 0:
                protein_type = 'non-membrane'
                membrane_thickness = 'nan'

            outpdb = '{}{}_{}.pdb'.format(pdbs_folder, pdb, chain)
            outpdb, chain = extract_chain(downloaded_pdb, chain, outpdb)
            #os.system('rm {}'.format(downloaded_pdb))
            return outpdb, protein_type, membrane_thickness   
        else:
            #print(" ... ... ... CA-only pdb")
            os.system('rm {}'.format(downloaded_pdb))
            return 'not available', 'not available', 'not available'
    else:
        return 'not available', 'not available', 'not available'

def get_all_pdbIDs(input_files, input_types):

    print("1. Getting all PDB ids associated with the input strings")

    pdbIDs = []

    for i, input_string in enumerate(input_files):
        if input_types[i] == 'pdb_file':
            pdb_file = input_string
            pdbIDs.append(pdb_file)
        elif input_types[i] == 'pdb_folder':
            pdb_files = ['{}/{}'.format(input_string, f) for f in os.listdir(input_string) if f.endswith('.pdb') and 'barrelOnly' not in f and 'moved_to_reference' not in f]
            pdbIDs += pdb_files
        elif input_types[i] == 'pdbID':
            pdbIDs.append(input_string)
        elif input_types[i] == 'hhpred':
            pdbs = parse_hhpred_output(input_string)
            for pdb in pdbs:
                if pdb not in pdbIDs:
                    pdbIDs.append(pdb)
        elif input_types[i] == 'pdbsum_fasta':
            pdbs = parse_pdbsum_fasta(input_string)
            for pdb in pdbs:
                if pdb not in pdbIDs:
                    pdbIDs.append(pdb)
        elif input_types[i] == 'folder':
            pdbs = ['{}/{}'.format(input_string, f) for f in os.listdir(input_string)]
            for pdb in pdbs:
                if not any(pdb in string for string in pdbIDs):
                    pdbIDs.append(pdb)
    
    return list(set(pdbIDs))

## 2.3. Functions to parallelise jobs

def chunk_list(l, n):
    chunks = array_split(array(l), n)
    chunks = [list(chunk) for chunk in chunks]
    return chunks

## 3. PDB FILE UTILITIES

## 3.1. Functions to parse pdb files

def parse_PDB_for_chain(pdb_file, chainID, outpdb):

    out = open(outpdb, 'w')

    found_chain = False
    found_endmdl = False
    with open(pdb_file, 'r') as inpdb:
        for line in inpdb:
            if (line.startswith('ATOM ') or (line.startswith('HETATM') and 'MSE' in line)) and not found_endmdl:
                chain = line[21]
                if chain == chainID:
                    found_chain = True
                    out.write(line)
            elif line.startswith('CONNECT') and not found_endmdl:
                out.write(line)
            elif line.startswith('ENDMDL'):
                found_endmdl = True

    if not found_chain:
        os.system('rm {}'.format(outpdb))

    return found_chain

def parse_membrane_thickness_from_OMP(pdb_file):

    thickness = 0.0

    with open(pdb_file, 'r') as pdbin:
        for line in pdbin:
            if line.startswith('REMARK') and '1/2 of bilayer thickness:' in line:
                thickness = float(line[40:49].strip())*2.0
                return thickness
            
    return thickness

## 3.2. Functions to write pdb files

def add_pseudoatoms(pdb_file, pos_coord, name='DUM', atmtype='N', remove_inpdb = False):

    newpdb = '{}_pseudoatom'.format(pdb_file[:-4])
    outpdb = open(newpdb, 'w')

    with open(pdb_file, 'r') as inpdb:
        for line in inpdb:
            if line.startswith('ATOM'):
                atomnum = int(line[7:12].strip())
                outpdb.write(line)
            elif 'HETATM' in line and 'DUM' in line:
                atomnum = int(line[7:12].strip())
                outpdb.write(line)

    for atom in pos_coord:
        line = 'HETATM {:4d}  {}   {}  {:4d}     {:7.3f} {:7.3f} {:7.3f}\n'.format(atomnum, atmtype, name, atomnum, atom[0], atom[1], atom[2])
        outpdb.write(line)
        atomnum += 1
        
    outpdb.close()

    if remove_inpdb:
        os.system('rm {}'.format(pdb_file))
        
    return newpdb

## 3.3. Functions to check pdb files

def is_CA_only(pdb_file):

    is_CAonly = True

    with open(pdb_file, 'r') as inpdb:
        for line in inpdb:
            if line.startswith('ATOM '):
                if 'CB' in line:
                    is_CAonly = False

    return is_CAonly

## 3.4. Functions to extract information from pdb files

def get_chains_in_pdb(pdb_file, source_pdb=True):
    
    if source_pdb and not pdb_file.endswith('.ent'):
        pdbs_folder, pdb = '/'.join(pdb_file.split('/')[:-1]), pdb_file.split('/')[-1].split('_')[0]
        downloaded_pdb = '{}/pdb{}.ent'.format(pdbs_folder, pdb.lower())
        if os.path.isfile(downloaded_pdb):
            pdb_file = downloaded_pdb
    
    chains = []
    found_endmdl = False
    with open(pdb_file, 'r') as inpdb:
        for line in inpdb:
            if (line.startswith('ATOM ') or (line.startswith('HETATM') and 'MSE' in line)) and not found_endmdl:
                chain = line[21]
                chains.append(chain)
            elif line.startswith('ENDMDL'):
                found_endmdl = True
    
    return sorted(list(set(chains))), pdb_file

def extract_chain(pdb_file, chainID, outpdb):

    #print(" ... Extracting chain {} from {}".format(chainID, pdb_file))
    found_chain = False
    found_chain = parse_PDB_for_chain(pdb_file, chainID, outpdb)
    
    if not found_chain:
        chains_in = {}
        chains_present = []
        
        with open(pdb_file,'r') as inpdb:
            for line in inpdb:
                if 'SEQRES   1' in line:
                    curr_chain = line[11:13].strip()
                    curr_len = line[13:17].strip()
                    if curr_len in chains_in.keys():
                        chains_in[curr_len].append(curr_chain)
                    else:
                        chains_in[curr_len] = [curr_chain]

                elif line.startswith('ATOM'):
                    curr_chain = line[20:22].strip()
                    if curr_chain not in chains_present:
                        chains_present.append(curr_chain)
        
        if len(chains_in) != 0 and chainID in chains_present:
            if len(chains_present) == 1:
                chainID = chains_present[0]
            elif len(chains_in) == 1:
                chainID = chains_present[0]
            else:
                for curr_len in chains_in.keys():
                    if chainID in chains_in[curr_len]:
                        chainID = set(chains_in[curr_len]) - (set(chains_in[curr_len]) - set(chains_present))
                        chainID = list(chainID)[0]
                        
            if chainID != '':
                outpdb, chainID = extract_chain(pdb_file, chainID, outpdb)
            else:
                return 'not available', 'not available'
        else:
            return 'not available', 'not available'
    
    else:
        new_chainID = chainID
        chains_in = {}
        chains_present = []
        with open(pdb_file,'r') as inpdb:
            for line in inpdb:
                if 'SEQRES   1' in line:
                    curr_chain = line[11:13].strip()
                    curr_len = line[13:17].strip()
                    if curr_len in chains_in.keys():
                        chains_in[curr_len].append(curr_chain)
                    else:
                        chains_in[curr_len] = [curr_chain]

                elif line.startswith('ATOM'):
                    curr_chain = line[20:22].strip()
                    if curr_chain not in chains_present:
                        chains_present.append(curr_chain)

        if len(chains_present) == 1:
            new_chainID = chains_present[0]
        elif len(chains_in) == 1:
            new_chainID = chains_present[0]
        else:
            for curr_len in chains_in.keys():
                if chainID in chains_in[curr_len]:
                    new_chainID = set(chains_in[curr_len]) - (set(chains_in[curr_len]) - set(chains_present))
                    new_chainID = sort(list(new_chainID))[0]

        if new_chainID != chainID:
            outpdb, chainID = extract_chain(pdb_file, new_chainID, outpdb)

    return outpdb, chainID

def get_CA_coordinates(pdb_file):

    CA_coordinates = []
    res_nums = []

    skip_next_CA = False
    prev_resnum = ''
    with open(pdb_file, 'r') as inpdb:
        for line in inpdb:
            #if (line.startswith('ATOM') or (line.startswith('HETATM') and 'MSE' in line)) and ' CA ' in line and line[20:22].strip() == chain:
            if (line.startswith('ATOM') or (line.startswith('HETATM') and ('MSE' in line or 'SCY' in line))) and ' CA ' in line:
                if not skip_next_CA:
                    coordinates = [line[30:38], line[38:46], line[46:54]]
                    coordinates = [float(i.strip()) for i in coordinates]
                    CA_coordinates.append(coordinates)

                    resnum = int(line[22:26].strip())
                    res_nums.append(resnum)

                    resname = line[16:21].strip()

                    prev_resnum = resnum
                    if len(resname) > 3:
                        skip_next_CA = True
                else:
                   resname = line[16:21].strip()
                   resnum = int(line[22:26].strip())
                   if len(resname) == 3 or resnum != prev_resnum:
                       coordinates = [line[30:38], line[38:46], line[46:54]]
                       coordinates = [float(i.strip()) for i in coordinates]
                       CA_coordinates.append(coordinates)

                       res_nums.append(resnum)
                       
                   skip_next_CA = False 

    return CA_coordinates, res_nums

def get_CB_coordinates(pdb_file):

    CB_coordinates = []
    res_nums = []

    skip_next_CB = False
    prev_resnum = ''
    with open(pdb_file, 'r') as inpdb:
        for line in inpdb:
            #if (line.startswith('ATOM') or (line.startswith('HETATM') and 'MSE' in line)) and ' CA ' in line and line[20:22].strip() == chain:
            if (line.startswith('ATOM') or (line.startswith('HETATM') and 'MSE' in line)):
                if ' CB ' in line and 'GLY' not in line:
                    if not skip_next_CB:
                        coordinates = [line[30:38], line[38:46], line[46:54]]
                        coordinates = [float(i.strip()) for i in coordinates]
                        CB_coordinates.append(coordinates)

                        resnum = int(line[22:26].strip())
                        res_nums.append(resnum)

                        resname = line[16:21].strip()

                        prev_resnum = resnum
                        if len(resname) > 3:
                            skip_next_CB = True
                    else:
                       resname = line[16:21].strip()
                       resnum = int(line[23:26].strip())
                       if len(resname) == 3 or resnum != prev_resnum:
                           coordinates = [line[30:38], line[38:46], line[46:54]]
                           coordinates = [float(i.strip()) for i in coordinates]
                           CB_coordinates.append(coordinates)

                           res_nums.append(resnum)
                           
                       skip_next_CB = False 

                elif ' CA' in line and 'GLY' in line:
                    resnum = int(line[22:26].strip())
                    if resnum not in res_nums:
                        res_nums.append(resnum)
                        coordinates = [nan, nan, nan]
                        CB_coordinates.append(coordinates)

    return CB_coordinates, res_nums

def get_backbone_coordinates(pdb_file, chain):

    bb_coordinates = []
    res_nums = []

    skip_next = False
    prev_resnum = ''
    with open(pdb_file, 'r') as inpdb:
        for line in inpdb:
            if (line.startswith('ATOM') or (line.startswith('HETATM') and 'MSE' in line)) and line[20:22].strip() == chain:
                if ' CA ' in line or line[12:16].strip() == 'N' or line[12:16].strip() == 'C' or line[12:16].strip() == 'O': 
                    if not skip_next:
                        coordinates = [line[30:38], line[38:46], line[46:54]]
                        coordinates = [float(i.strip()) for i in coordinates]
                        bb_coordinates.append(coordinates)

                        resnum = int(line[22:26].strip())
                        res_nums.append(resnum)

                        resname = line[16:21].strip()
                        
                        if len(resname) > 3:
                            skip_next = True
                        if ' O  ' in line:
                            prev_resnum = resnum
                    else:
                       resname = line[16:21].strip()
                       if len(resname) == 3 or resnum != prev_resnum:
                           coordinates = [line[30:38], line[38:46], line[46:54]]
                           coordinates = [float(i.strip()) for i in coordinates]
                           bb_coordinates.append(coordinates)

                           resnum = int(line[23:26].strip())
                           res_nums.append(resnum)
                           
                       if ' O  ' in line:   
                           skip_next = False 

    return bb_coordinates, res_nums

def get_backbone_carbons_coordinates(pdb_file, chain):

    bb_coordinates = []
    res_nums = []

    skip_next = False
    prev_resnum = ''
    with open(pdb_file, 'r') as inpdb:
        for line in inpdb:
            if (line.startswith('ATOM') or (line.startswith('HETATM') and 'MSE' in line)) and line[20:22].strip() == chain:
                if ' CA ' in line or line[12:16].strip() == 'C' : 
                    if not skip_next:
                        coordinates = [line[30:38], line[38:46], line[46:54]]
                        coordinates = [float(i.strip()) for i in coordinates]
                        bb_coordinates.append(coordinates)

                        resnum = int(line[22:26].strip())
                        res_nums.append(resnum)

                        resname = line[16:21].strip()
                        if len(resname) > 3:
                            skip_next = True

                        if ' C  ' in line:
                            prev_resnum = resnum
                    else:
                       resname = line[16:21].strip()
                       if len(resname) == 3 or resnum != prev_resnum:
                           coordinates = [line[30:38], line[38:46], line[46:54]]
                           coordinates = [float(i.strip()) for i in coordinates]
                           bb_coordinates.append(coordinates)

                           resnum = int(line[23:26].strip())
                           res_nums.append(resnum)
                       if ' C  ' in line:   
                           skip_next = False 

    return bb_coordinates, res_nums
    
## 3.5. Functions to calculate pdb properties

def get_secondary_structure(pdb_file):

    print(" ... ... ... Getting secondary structure from '{}'".format(pdb_file.split('/')[-1]))

    out_file = "{}_dssp.out".format(pdb_file)

    if not os.path.isfile(out_file):
        # try:
        #     os.system("mkdssp -i {} -o {}".format(pdb_file, out_file))
        # except:
        #     os.system("dssp -i {} -o {}".format(pdb_file, out_file))

        run_dssp = sp.Popen(['mkdssp', '-i', pdb_file, '-o', out_file], stdout=sp.PIPE, stderr=sp.PIPE, stdin=sp.PIPE)
        stdout, stderr = run_dssp.communicate() 
        if len(stderr) > 0:
            run_dssp = sp.Popen(['dssp', '-i', pdb_file, '-o', out_file], stdout=sp.PIPE, stderr=sp.PIPE, stdin=sp.PIPE)
            stdout, stderr = run_dssp.communicate() 
            if len(stderr) > 0:
                print(stderr)
                exit()
                   
    dssp_ss = parse_DSSPout(out_file)['SecStr']
    dssp_aa = parse_DSSPout(out_file)['AA']
    dssp_ch = parse_DSSPout(out_file)['Chains']
    seqstruct = ''
    sequence = ''
    chains = ''
    content = {}
    
    for i in range(len(dssp_ss)):
        seqstruct += dssp_ss[i]
        sequence += dssp_aa[i]
        chains += dssp_ch[i]

    #os.system("rm {}".format(out_file))
    
    return seqstruct, sequence, chains

def find_if_barrel_crosses_membrane(pdb_sequence, pdb_file, barrel_topology, membrane_thickness, max_loop_size = 5):

    barrel_crosses_membrane = False

    barrel_seq, barrel_struct = remove_long_loops_from_barrel(pdb_sequence, pdb_file, barrel_topology, max_loop_size = max_loop_size)
    CA_atoms, res_nums = get_CA_coordinates(barrel_struct)

    z_coordinates = [atom[2] for atom in CA_atoms]

    # if min(z_coordinates) <= 0.0-(membrane_thickness/2.0) and max(z_coordinates) >= 0.0+(membrane_thickness/2.0):
    if min(z_coordinates) <= 0.0 and max(z_coordinates) >= 0.0:
        barrel_crosses_membrane = True

    return barrel_crosses_membrane, barrel_struct

## 4. FUNCTIONS TO CALCULATE THE TOPOLOGY

## 4.1. Helper functions

def extract_regular_regions(topology, pdb_file, return_coord = False, marker = 'M', add_to_pdb = True, chainID = 'A', offset = 1, step = 2, size_threshold = 3):

    regular_regions = []
    count = 0
    size = 0

    foundone = False
    for i in range(len(topology)):
        if topology[i] == marker:
            if not foundone:
                start = i
            foundone = True
            size += 1
        if topology[i] != marker or i == len(topology)-1:
            if foundone:
                end = i
                count += 1
                if size > size_threshold:
                    regular_regions.append([start, end])
                size = 0
            foundone = False

    if add_to_pdb:
        CA_coordinates, local_angles, res_nums = findRR(pdb_file, chainID, offset = offset, step = step)

        start_coords = []
        end_coords = []
        
        for region in regular_regions:
            start_coords.append(CA_coordinates[region[0]])
            end_coords.append(CA_coordinates[region[1]])

        pdb_with_pseudoatoms = add_pseudoatoms(pdb_file, start_coords, name='DUM', atmtype='N')
        pdb_with_pseudoatoms = add_pseudoatoms(pdb_with_pseudoatoms, end_coords, name='DUM', atmtype='O', remove_inpdb = True)

    if return_coord:
        regular_regions_coordinates = {str(region): [] for region in regular_regions}
        regular_regions_resnums = {str(region): [] for region in regular_regions}
        
        CA_coordinates, res_nums = get_CA_coordinates(pdb_file)

        for region in regular_regions:
            for residue_index in range(region[0], region[1]+1):
                regular_regions_coordinates[str(region)].append(CA_coordinates[residue_index])
                regular_regions_resnums[str(region)].append(res_nums[residue_index])
                      
        return regular_regions_coordinates, regular_regions_resnums
    else:
        return regular_regions

def compute_local_angles(CA_coordinates, res_nums, offset, step):

    angles = []
    for i in range(len(CA_coordinates)-offset-step):
        if is_continuous(res_nums[i:i+offset+step]):                  
            curr_vector = array(CA_coordinates[i+step]) - array(CA_coordinates[i])
            next_vector = array(CA_coordinates[i+offset+step]) - array(CA_coordinates[i+offset])            
            vectors_angle = angle(curr_vector, next_vector, in_degrees = True)
        else:
            vectors_angle = nan
        angles.append(vectors_angle)

    return array(angles)

def findRR(pdb_file, chain, offset = 4, step = 2):
    
    CA_coordinates, res_nums = get_CA_coordinates(pdb_file)
    local_angles = compute_local_angles(CA_coordinates, res_nums, offset, step)

    return CA_coordinates, local_angles, res_nums

def process_findRR_angles(local_angles, offset, angle_threshold = 25):

    membtopo = ''
    for i in range(offset):
        membtopo += '-'

    for angle in local_angles:
        if angle <= angle_threshold:
            membtopo += 'M'
        else:
            membtopo += '-'

    for i in range(offset+1):
        membtopo += '-'

    for i in range(1,len(membtopo)-1):
        if membtopo[i] == 'M':
            if membtopo[i-1] == '-' and membtopo[i+1] == '-':
                s = list(membtopo)
                s[i] = '-'
                membtopo = "".join(s)
            elif i > 1 and i < len(membtopo)-2:
                if membtopo[i-1] == '-' and membtopo[i+1] == 'M' and membtopo[i+2] == '-':
                    s = list(membtopo)
                    s[i] = '-'
                    membtopo = "".join(s)
                elif membtopo[i+1] == '-' and membtopo[i-1] == 'M' and membtopo[i-2] == '-':
                    s = list(membtopo)
                    s[i] = '-'
                    membtopo = "".join(s)
                    
    for i in range(1,len(membtopo)-1):
        if membtopo[i] == '-':
            if membtopo[i-1] == 'M' and membtopo[i+1] == 'M':
                s = list(membtopo)
                s[i] = 'M'
                membtopo = "".join(s)
            elif i > 1 and i < len(membtopo)-2:
                if membtopo[i-1] == 'M' and membtopo[i+1] == '-' and membtopo[i+2] == 'M':
                    s = list(membtopo)
                    s[i] = 'M'
                    membtopo = "".join(s)
                elif membtopo[i+1] == 'M' and membtopo[i-1] == '-' and membtopo[i-2] == 'M':
                    s = list(membtopo)
                    s[i] = 'M'
                    membtopo = "".join(s)
        
    return membtopo

def write_topo_from_regular_regions(cycling_regions, strlength, marker = 'M'):

    membtopo = ''
    state = '-'
    for i in range(strlength):
        for region in cycling_regions:
            if i == region[0]:
                state = marker
            elif i == region[1]:
                state = '-'
        membtopo += state
        
    return membtopo

def compute_contacts_between_regular_regions(regular_regions, CA_coordinates, mode = 'min'):

    angles = []

    for regionA in regular_regions:
        curr_angles = []
        for regionB in regular_regions:
            if regionA != regionB:
                if regionA[0] < regionB[0]:
                    vectors_angle = minimun_distance_between_regions(regionA, regionB, CA_coordinates, mode = mode)
                else:
                    vectors_angle = minimun_distance_between_regions(regionB, regionA, CA_coordinates, mode = mode)
            else:
                vectors_angle = nan
            curr_angles.append(vectors_angle)            
        angles.append(curr_angles)

    return array(angles)

def minimun_distance_between_regions(regionA, regionB, CA_coordinates, mode = 'min'):

    min_dist = []

    for j in range(regionA[0]+1, regionA[1]): # does not allow the N and C-terminal residues, in order to prevent 'in line' contacts
        coordA = array(CA_coordinates[j])
        dist = []
        for i in range(regionB[0]+1, regionB[1]):
            if i >= len(CA_coordinates):
                i = len(CA_coordinates)-1
                
            coordB = array(CA_coordinates[i])
            distance = 0
            for a in range(len(coordA)):
                distance += (coordA[a] - coordB[a])*(coordA[a] - coordB[a])
            distance = sqrt(distance)
            dist.append(distance)

        if len(dist) > 0:
            min_dist.append(min(dist))

    try:
        if mode == 'min':
            return min(min_dist)
        elif mode == 'median':
            return median(min_dist)
        elif mode == 'mean':
            return mean(min_dist)
    except:
        return nan
    
def get_adjacency_matrix_from_contacts(distance_matrix, distance_threshold = 10):

    contact_matrix = [[0 for x in range(len(distance_matrix[1]))] for y in range(len(distance_matrix))]
    
    for i in range(len(distance_matrix)):
        for j in range(len(distance_matrix[i])):   
            if distance_matrix[i][j] >= distance_threshold or isnan(distance_matrix[i][j]):
                contact_matrix[i][j] = 0
            else:
                contact_matrix[i][j] = 1

    return contact_matrix

def find_best_regions_in_cycle(regions_in_cycle, regular_regions, CA_coordinates):

    best_regions_in_cycle = []

    for i, regions in enumerate(regions_in_cycle):
        if len(best_regions_in_cycle) < len(regions):
            best_regions_in_cycle = regions
            
        elif len(best_regions_in_cycle) == len(regions):
            
            intersection = list(set(best_regions_in_cycle) & set(regions))

            mean_z_best = mean([mean([CA_coordinates[j][-1] for j in range(regular_regions[k][0], regular_regions[k][-1])]) for k in best_regions_in_cycle if k not in intersection])
            mean_z_current = mean([mean([CA_coordinates[j][-1] for j in range(regular_regions[k][0], regular_regions[k][-1])]) for k in regions if k not in intersection])

            if abs(mean_z_current) < abs(mean_z_best):
                best_regions_in_cycle = regions

    return best_regions_in_cycle

def process_regions_in_cycle(regions_in_cycle, regular_regions):

    cycling_regions = []

    for i in range(len(regular_regions)):
        if i in regions_in_cycle and regular_regions[i] not in cycling_regions:
                cycling_regions.append(regular_regions[i])

    return cycling_regions

def list_matcher(lst1, lst2):
    match = []
    for i, elementi in enumerate(lst1):
        for j, elementj in enumerate(lst2):
            if elementi == elementj:
                if i < len(lst1)-1 and j < len(lst2)-1:
                    if lst1[i+1] == lst2[j+1]:
                        if lst1[i] not in match:
                            match.append(lst1[i])
                        if lst1[i+1] not in match:
                            match.append(lst1[i+1])
    return match

def find_additional_cycle(cycl1, cycl2):
    
    double_cycl1 = cycl1*2
    double_cycl2 = cycl2*2

    buble = list_matcher(double_cycl1, double_cycl2)

    masked_cycl1 = [i if i not in buble[1:-1] else 'new_elements_here' for i in double_cycl1]

    new_elements = []
    found_new_elements = False
    for element in double_cycl2:
        if element == buble[-1]:
            found_new_elements = True
        elif element == buble[0]:
            found_new_elements = False
        elif found_new_elements and element not in new_elements:
            new_elements.append(element)

    new_elements.reverse()

    new_cycle = []
    for i in cycl1:
        if i not in buble[1:-1]:
            new_cycle.append(i)
        else:
            for j in new_elements:
                if j not in new_cycle:
                    new_cycle.append(j)

    new_cycle.reverse()

    return new_cycle

## 4.2. Main functions

def process_barrel(pdb_file, ID, chainID, simplify = False, offset = 1, step = 2, local_angle_threshold = 25, distance_threshold = 5, round=0, multimer_only=False):

    # get barrel topology from combined Regular Regions and DSSP
    seqstruct, pdb_sequence, barrel_topology, chains = get_barrel_topology(pdb_file, ID = ID, chainID = chainID, mode = 'combined', offset = offset, step = step, local_angle_threshold = local_angle_threshold, distance_threshold = distance_threshold)
    # find number of transmembrane sections
    num_bb_regions = find_number_of_regions_in_barrel(barrel_topology)

    print(barrel_topology)

    if num_bb_regions == 0 or num_bb_regions % 2 != 0: # try again but now using regular regions only
        seqstruct, pdb_sequence, barrel_topology, chains = get_barrel_topology(pdb_file, ID = ID, chainID = chainID, mode = 'from regular regions', offset = offset, step = step, local_angle_threshold = local_angle_threshold, distance_threshold = distance_threshold)
        num_bb_regions = find_number_of_regions_in_barrel(barrel_topology)
    if num_bb_regions == 0 or num_bb_regions % 2 != 0: # try again but now using DSSP only
        seqstruct, pdb_sequence, barrel_topology, chains = get_barrel_topology(pdb_file, ID = ID, chainID = chainID, mode = 'from DSSP', offset = offset, step = step, distance_threshold = distance_threshold)
        num_bb_regions = find_number_of_regions_in_barrel(barrel_topology)
    
    # if no barrel was found, check if it could be formed by multiple chains if multimer only is turned on
    if num_bb_regions == 0 and round == 0 and multimer_only:
        chains_in_pdb, pdb_file = get_chains_in_pdb(pdb_file)
        pdb_sequence, barrel_topology, chains, num_bb_regions, _ = process_barrel(pdb_file, ID, chains_in_pdb, simplify = simplify, offset = offset, step = step, local_angle_threshold = local_angle_threshold, distance_threshold = distance_threshold, round=1)
        
    return pdb_sequence, barrel_topology, chains, num_bb_regions, pdb_file

def get_barrel_topology(pdb_file, ID, chainID, simplify = False, mode = 'combined', offset = 1, step = 2, local_angle_threshold = 25, distance_threshold = 5):

    print(" ... ... Getting barrel topology for '{}' using mode '{}'".format(pdb_file, mode))
    
    seqstruct, pdb_sequence, chains = get_secondary_structure(pdb_file)

    if mode == 'from DSSP':
        CA_coordinates, res_nums = get_CA_coordinates(pdb_file)
        regular_regions = extract_regular_regions(seqstruct, pdb_file, marker = 'E', add_to_pdb = False, chainID = chainID)
        
    elif mode == 'from regular regions':

        # calculate first with the step provided (local angles)
        CA_coordinates, local_angles, res_nums = findRR(pdb_file, chainID, offset = offset, step = step)
        membtopo = process_findRR_angles(local_angles, offset, angle_threshold = local_angle_threshold)
        regular_regions = extract_regular_regions(membtopo, pdb_file, marker = 'M', add_to_pdb = False, chainID = chainID, offset = offset, step = step)
        membtopo = write_topo_from_regular_regions(regular_regions, strlength = len(CA_coordinates))
        
    elif mode == 'combined':
        # get regular regions from angles
        CA_coordinates, local_angles, res_nums = findRR(pdb_file, chainID, offset = offset, step = step)
        topo_fromRR = process_findRR_angles(local_angles, offset, angle_threshold = local_angle_threshold)
        regular_regions_fromRR = extract_regular_regions(topo_fromRR, pdb_file, marker = 'M', add_to_pdb = False, chainID = chainID, offset = offset, step = step)
        topo_fromRR = write_topo_from_regular_regions(regular_regions_fromRR, strlength = len(CA_coordinates))

        # plot the variation of the local angles along the structure
        plt.clf()
        plt.plot(local_angles)
        plt.ylabel(r'Local angles')
        plt.xlabel('Residue')
        plt.savefig('{}_local_angles.pdf'.format(pdb_file))

        # get regular regions as the strands in DSSP
        regular_regions_DSSP = extract_regular_regions(seqstruct, pdb_file, marker = 'E', add_to_pdb = False, chainID = chainID)
        topo_fromDSSP = write_topo_from_regular_regions(regular_regions_DSSP, strlength = len(CA_coordinates))

        # combine the topologies by extending the regular regions from angles with DSSP regular regions and get the new regular regions        
        membtopo = ''
        for i in range(len(topo_fromRR)):
            state = topo_fromRR[i]
            if topo_fromRR[i] == '-':
                if topo_fromDSSP[i] == 'M':
                    state = 'M'
            membtopo += state        

        regular_regions = extract_regular_regions(membtopo, pdb_file, marker = 'M', add_to_pdb = False, chainID = chainID)

    contacts_between_regular_regions = compute_contacts_between_regular_regions(regular_regions, CA_coordinates, mode = 'min')

    if len(contacts_between_regular_regions) > 1:
        draw_contacts_heatmap(contacts_between_regular_regions, fig_label='{}'.format(pdb_file[:-4]), labels = range(1,len(regular_regions)+1))
    
        contact_matrix = get_adjacency_matrix_from_contacts(contacts_between_regular_regions, distance_threshold = distance_threshold)
        graph = graph_from_adjacency_matrix(contact_matrix)

        draw_contacts_heatmap(contact_matrix, fig_label='{}_contacts'.format(pdb_file[:-4]), labels = range(1,len(regular_regions)+1), colormap = 'Greys')
        
        try:
            regions_in_cycle = nx.cycle_basis(graph)
            
            # update by adding the extra cycle that may be missing
            if len(regions_in_cycle) > 1:
                for i in range(len(regions_in_cycle)-1):
                    region1 = regions_in_cycle[i]
                    region2 = regions_in_cycle[i+1]

                    if len(list_matcher(region1*2, region2*2)) > 2:
                        additional_cycle = find_additional_cycle(region1, region2)
                        if additional_cycle not in regions_in_cycle:
                            regions_in_cycle.append(additional_cycle)
                     
            regions_in_cycle.sort(key=len)
            regions_in_cycle = find_best_regions_in_cycle(regions_in_cycle, regular_regions, CA_coordinates)

        except:
           regions_in_cycle = []

        regionsBB = process_regions_in_cycle(regions_in_cycle, regular_regions)
        membtopo = write_topo_from_regular_regions(regionsBB, strlength = len(CA_coordinates))
        regular_regions = extract_regular_regions(membtopo, pdb_file, marker = 'M', add_to_pdb = True, chainID = chainID)
        try:
            draw_graph(graph, labels = list(range(1,len(regular_regions)+1)), fig_label='{}'.format(pdb_file[:-4]))
        except:
            print('Not possible to draw. Maybe due to a networkx issue. Will skip it')
    
    else:
        membtopo = ''
        for i in range(len(seqstruct)):
            membtopo += '-'
        
    return seqstruct, pdb_sequence, membtopo, chains

def find_number_of_regions_in_barrel(membrane_topology):

    n = 0
    for i in range(len(membrane_topology)):
        if membrane_topology[i] == 'M' and membrane_topology[i-1] != 'M':
            n += 1
    return n

## 5. FUNCTIONS TO DEAL WITH AND CLASSIFY LOOPS

def get_chains_topology(topology, chains):

    chains_topo = {}
    for i, ch in enumerate(chains):
        if ch not in chains_topo:
            chains_topo[ch] = ''
        try:
            chains_topo[ch] += topology[i]
        except:
            pass
    return chains_topo

def get_chains_sequence(pdb_sequence, chains):

    chains_seq = {}
    for i, ch in enumerate(chains):
        if ch not in chains_seq:
            chains_seq[ch] = ''
        try:
            chains_seq[ch] += pdb_sequence[i]
        except:
            pass
    return chains_seq

def mask_loops(topology, chains):

    chains_topo = get_chains_topology(topology, chains)
    
    new_topology = []
    for ch in chains_topo:
        chains_topo[ch] = list(chains_topo[ch])
        found_n_term = False
        found_c_term = False
        for i, t in enumerate(chains_topo[ch]):
            if t == 'M':
                found_n_term = True
                if len(set(chains_topo[ch][i+1:])) == 1:
                    found_c_term = True

            elif found_n_term and not found_c_term:
                chains_topo[ch][i] = 'l'
        
        new_topology += chains_topo[ch]

    return ''.join(new_topology)

## 6. FUNCTIONS TO MODIFY AND ANALYSE THE BARREL

## 6.1. Helper functions

def organize_atoms_into_Z_bins(atoms_coordinates, max_z, min_z, step_z):

    bins = arange(min_z, max_z, step_z)
    z_bins = {i: [] for i in bins}

    for atom in atoms_coordinates:
        z_coord = atom[-1]
        for i in range(1, len(bins)):
            if z_coord < bins[i] and z_coord > bins[i-1]:
                z_bins[bins[i]].append(atom)
            
    return z_bins

def get_loops_to_remove(pdb_sequence, pdb_file, topology, max_loop_size = 5, chains=None, len_prev_chain = 0):

    if chains is not None:
        chains_topology = get_chains_topology(topology, chains)
        chains_sequence = get_chains_sequence(pdb_sequence, chains)

        loops_to_remove = []
        for chain in chains_topology:
            curr_loops = get_loops_to_remove(chains_sequence[chain], pdb_file, chains_topology[chain], max_loop_size=max_loop_size, len_prev_chain=len_prev_chain)            
            loops_to_remove += curr_loops
            len_prev_chain += len(chains_topology[chain])

    else:
        if len(set(topology)) == 1 and topology[0]=='-':
            loops_to_remove = [[0+len_prev_chain,len(topology)+len_prev_chain]]
        else:
            loops_to_remove = extract_regular_regions(topology, pdb_file, marker = '-', add_to_pdb = False, size_threshold = max_loop_size)
            loops_to_remove = add_pad_to_loops(loops_to_remove, max_loop_size, pdb_sequence, len_prev_chain)
        
    return loops_to_remove

def add_pad_to_loops(loops_to_remove, max_loop_size, pdb_sequence, len_prev_chain):

    for loop in loops_to_remove:
        if loop[0] == 0:
            loop[1] = loop[1]-int(max_loop_size)/2
        elif loop[1] == len(pdb_sequence)-1:
            loop[0] = loop[0]+int(max_loop_size)/2
        else:
            loop[0] = loop[0]+int(max_loop_size)/2
            loop[1] = loop[1]-int(max_loop_size)/2
        loop[0] += len_prev_chain
        loop[1] += len_prev_chain

    return loops_to_remove

## 6.2. Main functions

def remove_long_loops_from_barrel(pdb_sequence, pdb_file, topology, max_loop_size = 5, chains=None):

    print(" ... ... Getting barrel sequence for '{}', excluding non-barrel residues in segments 'loops' longer than {} residues".format(pdb_file, max_loop_size))
    loops_to_remove = get_loops_to_remove(pdb_sequence, pdb_file, topology, max_loop_size = max_loop_size, chains=chains)

    new_pdb = '{}_barrelOnly.pdb'.format(pdb_file.replace('.pdb',''))

    outpdb = open(new_pdb, 'w')
    CA_coordinates, res_nums = get_CA_coordinates(pdb_file)
    seq = ''

    print(" ... ... ... len: {}".format(len(pdb_sequence)))

    # print(chains)
    # print(pdb_sequence)
    # print(topology)
    # print(loops_to_remove)

    for i in range(len(pdb_sequence)):
        in_loop = False
        
        for loop in loops_to_remove:
            if i >= loop[0] and i <= loop[1]:
                in_loop = True

        if not in_loop:
            seq += pdb_sequence[i]
            res_num_to_take = res_nums[i]
            if chains is not None:
                chain_to_take = chains[i]
            with open(pdb_file, 'r') as inpdb:
                for line in inpdb:
                    if line.startswith('ATOM') or (line.startswith('HETATM') and 'MSE' in line):
                        resnum = int(line[22:26].strip())
                        chain  = line[21:22].strip()
                        if resnum == res_num_to_take and (chains is None or chain == chain_to_take):
                            # print(resnum, res_num_to_take, pdb_sequence[i], line[:-1])
                            outpdb.write(line)
    outpdb.close()
                        
    return seq, new_pdb

def extract_regular_regions_end_coords(pdb_file, topology):

    regular_regions = extract_regular_regions(topology, pdb_file, marker = 'M', add_to_pdb = False, size_threshold = 0)
    CA_coordinates, res_nums = get_CA_coordinates(pdb_file)

    start_coords = []
    end_coords = []
    
    for region in regular_regions:
        start_coords.append(CA_coordinates[region[0]])
        end_coords.append(CA_coordinates[region[1]])

    return start_coords, end_coords

def move_barrel_to_reference_position(pdb_file, barrel_topology, center = [0,0,0], reference_axis_system = array([[1,0,0], [0,1,0], [0,0,1]])):

    # new_pdb = '{}_moved_to_reference.pdb'.format(pdb_file.split('.')[0])
    new_pdb = '{}_moved_to_reference.pdb'.format(pdb_file.replace('.pdb',''))

    outpdb = open(new_pdb, 'w')

    # Squash the barrel, which means get a perpendicular slice of the barrel at the center of it.
    CA_coordinates = squash_the_barrel(pdb_file, barrel_topology, atoms_per_region = 3)
    
    # Find translation vector (the mid position) and update the coordinates to center it at the requested position
    translation_vector = array([mean([atom[j] for atom in CA_coordinates]) for j in range(3)])
    CA_atoms_coords = array([[atom[j]-translation_vector[j]+center[j] for j in range(3)] for atom in CA_coordinates])
    
    # Carry out singular value decomposition (svd) for the coordinates in order to get the axes describing the best fitting plane and its normal vector
    svd = transpose(linalg.svd(CA_atoms_coords.T)) 
    barrel_axes = svd[0]
    barrel_axis = barrel_axes[-1]

    # Calculate rotation matrix
    # check pdf file 'RotMatrix.pdf' for the math behind the rotation matrix [thanks Joao Barros :)] 
    rotation_matrix = dot(linalg.inv(barrel_axes), reference_axis_system)

    curr_angle = angle(barrel_axis, array([0,0,1]), in_degrees=True, simplify=True)
    #print(' ######## Current angle: ',curr_angle)

    # Apply rotation matrix to the all atom coordinates and write pdb
    with open(pdb_file, 'r') as inpdb:
        for line in inpdb:
            if line.startswith('ATOM'):
                coordinates = [line[30:38], line[38:46], line[46:54]]
                coordinates = [float(i.strip()) for i in coordinates]
                coordinates = array([coordinates[j]-translation_vector[j]+center[j] for j in range(3)])
                rotated_coords = dot(rotation_matrix,coordinates)
                rotated_coords = '{:8.3f}{:8.3f}{:8.3f}'.format(rotated_coords[0], rotated_coords[1], rotated_coords[2])

                line = line.replace(line[30:54], rotated_coords)
            outpdb.write(line)

    outpdb.close()    
    
    return new_pdb, curr_angle

def squash_the_barrel(pdb_file, barrel_topology, atoms_per_region = 1, n_sigma = 1):

    CA_atoms, res_nums = get_CA_coordinates(pdb_file)
    regular_regions = extract_regular_regions(barrel_topology, pdb_file, marker = 'M', add_to_pdb = False, size_threshold = 0)

    center = array([mean([atom[j] for atom in CA_atoms]) for j in range(3)])

    coordinates = []

    for region in regular_regions:
        distances = []
        for i in range(region[0], region[1]):
            atom = CA_atoms[i]
            distance = linalg.norm(atom-center)
            distances.append([atom, distance])
        distances = array(distances)
        distances = distances[distances[:,1].argsort()]

        for j in range(0, atoms_per_region):
            try:
                coordinates.append(distances[j][0])
            except:
                continue

    return array(coordinates)

def get_barrel_diameter(pdb_file, chainID, min_residue_numb = 3, mode = 'CA', step_z = 1, membrane_thickness = 25):

    print(" ... ... Calculating the diameter of the barrel along the Z axis for '{}' using mode '{}'".format(pdb_file, mode))

    diameter = 0.0
    if mode == 'CA':
        atoms_coordinates, res_nums = get_CA_coordinates(pdb_file)
        atom_step = 1
        min_atom_numb = min_residue_numb*atom_step
    elif mode == 'backbone':
        atoms_coordinates, res_nums = get_backbone_coordinates(pdb_file, chainID)
        atom_step = 4
        min_atom_numb = min_residue_numb*atom_step
    elif mode == 'carbons':
        atoms_coordinates, res_nums = get_backbone_carbons_coordinates(pdb_file, chainID)
        atom_step = 2
        min_atom_numb = min_residue_numb*atom_step
    else:
        return 0.0

    print(" ... ... ... Considering only the Z bins that contain more than {} atoms".format(min_atom_numb))

    # find the maximum and minimum Z
    if membrane_thickness is None:
        membrane_thickness = 25

    max_z, min_z = (membrane_thickness/2.0)+step_z, ((-1)*membrane_thickness/2.0)-step_z

    # organize atoms into bins between the z maximum and the z minimum
    z_bins = organize_atoms_into_Z_bins(atoms_coordinates, max_z, min_z, step_z)
    
    # calculate the maximum distance of each atom to any other in the same Z bin
    z_distances = {z: [] for z in z_bins}
    
    for z in z_bins:
        atoms = z_bins[z]
        if len(atoms) > min_atom_numb:
            for coordA in atoms:
                distances = []
                for coordB in atoms:
                    if coordA != coordB:
                        distance = 0
                        for a in range(len(coordA)):
                            distance += (coordA[a] - coordB[a])*(coordA[a] - coordB[a])
                        distance = sqrt(distance)
                        distances.append(distance)
                if len(distances) > 0:
                    z_distances[z].append(max(distances))
                else:
                    z_distances[z].append(distances)

    # calculate the median maximum distance at each Z bin
    z_medians = []
    z_mads = []
    z_values = []

    for z in sort(list(z_bins.keys())):
        if len(z_distances[z]) >= min_atom_numb:
            med = median(z_distances[z])
            if not isnan(med):
                z_values.append(z)
                z_medians.append(med)
                z_mads.append(mad(z_distances[z]))

    if len(z_medians) > 0:
        plt.clf()
        plt.errorbar(z_values, z_medians, yerr = z_mads, color = 'black')
        plt.hlines(median(z_medians), min(z_values), max(z_values), linestyle='--')
        plt.ylabel(r'Median maximum distance between two residues ($\AA$)')
        plt.xlabel('Z')
        plt.savefig('{}_diameter_based_on_{}.pdf'.format(pdb_file, mode))

        # approximate the diameter of the barrel to the median of the median maximum distances along Z axis
        diameter = median(z_medians)
        
        return diameter, mad(z_medians)

    else:
        return None, None

def save_hairpins(pdb_sequence, pdb_file, barrel_topology, chains, marker='M', n=2, outfolder = 'fragments'):

    CA_atoms, res_nums = get_CA_coordinates(pdb_file)
    regular_regions = extract_regular_regions(barrel_topology, pdb_file, marker = marker, add_to_pdb = False, size_threshold = 0)

    register = find_motif_register(regular_regions, n)

    if register is not None:
        hairpins = [regular_regions[i:i+n] for i in range(register, len(regular_regions), n)]

        outfolder = '{}/{}'.format('/'.join(pdb_file.split('/')[:-1]), outfolder)
        if not os.path.isdir(outfolder):
            os.mkdir(outfolder)

        with open(pdb_file, 'r') as inpdb:
            for line in inpdb:
                if line.startswith('ATOM') or (line.startswith('HETATM') and 'MSE' in line):
                    resnum = int(line[22:26].strip())
                    chain  = line[21:22].strip()

                for i, hairpin in enumerate(hairpins):
                    if len(hairpin) == n:
                        outfile = '{}/{}_fragment{}.pdb'.format(outfolder, pdb_file.split('/')[-1].strip('.pdb'), i)
                        
                        start = res_nums[hairpin[0][0]]
                        end = res_nums[hairpin[1][1]]

                        if resnum >= start-1 and resnum <= end+1:
                            with open(outfile, 'a+') as outpdb:
                                outpdb.write(line)


def find_motif_register(regular_regions, n):

#   It finds the register that allows for the shorter linker between units

    starting_motifs = regular_regions[:n+1]
    linkers = [starting_motifs[i+1][0]-starting_motifs[i][1] for i in range(len(starting_motifs)-1)]

    try:
        return linkers.index(max(linkers))
    except:
        return None

## 7. BASIC MATH-RELATED FUNCTIONS

## 7.1. Graphs

def graph_from_adjacency_matrix(adjacency_matrix):

    rows, cols = where(array(adjacency_matrix) == 1)
    edges = zip(rows.tolist(), cols.tolist())
    gr = nx.Graph()
    gr.add_edges_from(edges)
    
    return gr

## 7.2. General

def is_continuous(int_list):

    state = True
    for i in range(1, len(int_list)):
        if int_list[i] - int_list[i-1] > 1:
            state = False

    return state

## 7.3. Geometry and algebra

def dotproduct(v1, v2):
    dotp = sum((a*b) for a, b in zip(v1, v2))
    return dotp

def length(v):
    leng = sqrt(dotproduct(v, v))
    return leng

def angle(v1, v2, in_degrees = False, take_cos = False, simplify = False):
    ang = round(dotproduct(v1, v2) / (length(v1) * length(v2)), 10)
    ang = acos(ang)
    if simplify:
        if ang > (pi/2.0):
            v2 = v2*(-1)
            ang = angle(v1, v2)
    if in_degrees:
        ang = (ang * 180)/pi
    if take_cos:
        ang = cos(ang)
    return ang


## 7.4. Statistics

def mad(array):

    median_array = median(array)
    deviations = [abs(i-median_array) for i in array]

    return median(deviations)*1.4826

## 8. PLOTTING FUNCTIONS

def draw_contacts_heatmap(twodmatrix, fig_label, labels, colormap = 'CMRmap'):

    plt.clf()
    ax = sns.heatmap(twodmatrix, cmap = colormap, xticklabels = labels, yticklabels = labels)
    bottom, top = ax.get_ylim()
    ax.set_ylim(bottom + 0.5, top - 0.5)
    plt.savefig('{}_contacts.pdf'.format(fig_label))

    return

def draw_graph(graph, labels, fig_label):

    plt.clf()
    mylabels = dict(zip(graph, labels))
    nx.draw_circular(graph, node_size=500, labels=mylabels, with_labels=True, cmap = plt.cm.jet, node_color=range(graph.number_of_nodes()))
    plt.savefig('{}_graph.pdf'.format(fig_label))
    
    return

## 9. TO CALCULATE THE SHEAR NUMBER (Project by Roger Bamert)

def shear_number(pdb_file, show_path = False):
    
    # chains_in_barrel = get_chains_in_pdb(inpdb_file, source_pdb=False)[0]
    # pdb_file, _ = extract_chain(inpdb_file, chains_in_barrel[0], outpdb='/tmp/{}_{}'.format(inpdb_file.split('/')[-1], chains_in_barrel[0]))

    CA_coordinates, res_nums = get_CA_coordinates(pdb_file)
    regions = find_regions(res_nums)
    
    angles_matrix, alpha_angles = calculateAnglesMatrix(CA_coordinates, regions)
    adj_matrix = create_adjacency_matrix(CA_coordinates, angles_matrix, regions)
    
    connections = find_connections(CA_coordinates, adj_matrix ,res_nums, regions)
    sorted_connections = sort_connections_to_regions(connections, regions)
    all_paths = build_paths_from_connections(sorted_connections,regions)
    full_paths = build_path(all_paths, regions)
    
    if full_paths is not None:
        directions = sheet_directions(regions, CA_coordinates)
        if directions is not None:
            checked_paths = check_paths(full_paths, regions)
            if checked_paths is not None:
                all_shears = calculate_shear(checked_paths, directions, regions)
                shear = find_min_shear(all_shears)
        
                if show_path:
                    changed_to_res_num = []
                    for p in checked_paths:
                        changed_to_res_num.append(conversion_to_res_num(p, res_nums))
                    draw_paths(changed_to_res_num, regions, showStart = False, resinum = True)
                    draw_for_pymol(changed_to_res_num)
            else:
                print('... ... ...Error: All paths failed the last check')
                return None
        else:
            print('... ... ...Error: Could not calculate the directions') 
            return None
    else:
        print('........Error: Was not able to find or build any path')
        return None

    # os.system('rm {}'.format(pdb_file))

    return shear

## 9.1 Angle and adjacency Matrix construction

def find_regions(res_num):

    # Find the regions (beta sheets) throught the resdiues numbers
    
    regions = []
    helperindex = 0
    
    for index, res in enumerate(res_num):
        
        if index == 0:
            continue
        elif index == len(res_num)-1:
            regions.append((helperindex, index))
            break
        elif (res-res_num[index-1])>1:
            regions.append((helperindex,index-1))
            helperindex = index
        else : continue
            
    return regions

def calculateAnglesMatrix(CA_coordinates, regions, threshold = 6):

    # Calculate the angle between the Calpha vector and the H vector and store it in a matrix as well as calculating the angle between the Z-vector and the Calpha vector (alpha angle)
    # Threshold is the length of the H-vector 

    angles_matrix=[[0 for x in range(len(CA_coordinates))] for y in range(len(CA_coordinates))]
    
    alpha_angles = []
    curr_ca = 0
    
    for i, curr_region in enumerate(regions):
        if i < len(regions)-1:
            next_region = regions[i+1]
        else:
            next_region = regions[0]
        
        for i_ca in range(curr_region[0], curr_region[1]+1):
            curr_CA = CA_coordinates[i_ca]
            if i_ca < next_region[0]-3:
                C_vector = array(CA_coordinates[i_ca+2]) - array(curr_CA)
            else:
                C_vector = array(CA_coordinates[i_ca-2]) - array(curr_CA)
                        
            for j_ca in range(next_region[0], next_region[1]+1):
                next_CA = CA_coordinates[j_ca]
                H_vector = array(next_CA) - array(curr_CA)
                
                if length(H_vector) > threshold:
                    angles_matrix[i_ca][j_ca] = 0
                    angles_matrix[j_ca][i_ca] = 0
                else:
                    vector_angle = angle(C_vector, H_vector, in_degrees = True, simplify = True)
                    angles_matrix[i_ca][j_ca] = vector_angle
                    angles_matrix[j_ca][i_ca] = vector_angle
                
            alpha_angle = angle(C_vector, array([0,0,1]), in_degrees = True, simplify = True)
            alpha_angles.append(alpha_angle)
                
    return angles_matrix, alpha_angles

def create_adjacency_matrix(CA_coordinates, angles_matrix, regions, threshold=12):
    
    # Filter the angles matrix depending on the threshold. Change postion i,j to 1 if the angle is the same or higher than 90-threshold, anything below is 0.

    adj_matrix = [[0 for i in range(len(angles_matrix))] for j in range(len(angles_matrix))]
    first_start , first_end = regions[0]
    last_start, last_end = regions[-1]
    
    for i in range(len(angles_matrix)):
        for j in range(len(angles_matrix)):
            if angles_matrix[i][j] >= 90-threshold and adj_matrix[j][i] == 0 and i!=j:
                if j >= last_start and i >= first_start and i <= first_end:
                    adj_matrix[j][i] = 1
                else:
                    adj_matrix[i][j] = 1
            
    return adj_matrix

## 9.2 Finding all connections and paths. Building paths when necessary 

def find_connections(CA_coordinates, angles_matrix, res_nums, regions):

    # Extract all connections (only one as it is symmetrical) form the adjacency matrix
    
    connections = []
    first_start , first_end = regions[0]
    second_start , second_end = regions[1]
    
    for j, curr_CA in enumerate (CA_coordinates):
        
        for k, next_CA in enumerate (CA_coordinates):
            if angles_matrix[j][k] == 1:
                if k > second_end and j >= first_start and j <= first_end :
                    connections.append((k , j))
                else:
                    connections.append((j , k))
    return connections

def sort_connections_to_regions(connections, regions):
    
    # Sort the connections obtained form the adjacency matirx corresponding to the starting regions of the connection
    
    regiontracker = 0
    sorted_con = []
    region_con = []
    start, end = regions[regiontracker]
    
    for index, con in enumerate(connections):
        if con[0] <= end:
            region_con.append(con)
        else:
            regiontracker += 1
            start, end = regions[regiontracker]
            sorted_con.append(region_con)
            region_con = []
            region_con.append(con)
            
    sorted_con.append(region_con)
    
    return sorted_con
            
def build_paths_from_connections(sorted_connections,regions):

    # Builds together coherent paths from all connections (Uses recursivly function find_path to go furhter in the path)

    paths = []
    single_path = []
    matches = []
    regiontracker = 0
    start, end = regions[regiontracker]
    
    for index, sor_con in enumerate(sorted_connections):
        for k, con in enumerate(sor_con):
            single_path.append(con)
            regiontracker += 1
            if regiontracker >= len(sorted_connections)-1:
                regiontracker = 0
                
            single_path = (further_path(regiontracker, index, sorted_connections, con, single_path))

            if type(single_path[0])==list:
                for path in single_path:
                    paths.append(path)
                single_path =[]
            else:
                paths.append(single_path)
                single_path = []
                
            regiontracker = index
            
    return paths

def further_path(regiontracker, index, sorted_connections, con, path):
    
    # Recursively goes further the coherent path (used by build_paths_from_connections)
    
    if regiontracker == index:
        return path
    
    a, b = con
    matches = [match for match in sorted_connections[regiontracker] if match[0] == b]
    
    if not matches:
        return path
    elif len(matches) > 1:
        all_branched_paths = []
        
        for k, match in enumerate(matches):
            branched_path = path.copy()
            branched_path.append(match)
            
            if regiontracker == len(sorted_connections)-1:
                branched_path = further_path(0, index, sorted_connections, match, branched_path)
            else:
                branched_path = further_path(regiontracker+1, index, sorted_connections, match, branched_path)
            if type(branched_path[0])==list:
                for paths in branched_path:
                    all_branched_paths.append(paths)
            else:
                all_branched_paths.append(branched_path)
            branched_path = []
        path = []
        for paths in all_branched_paths:
            path.append(paths)
        all_branched_paths = [] 
        
        return path
    else:
        path.append(matches[0])
        
        if regiontracker == len(sorted_connections)-1:
            path = further_path(0, index, sorted_connections, matches[0], path)
        else:
            path = further_path(regiontracker+1, index, sorted_connections, matches[0], path)
        
        return path

def search_region(regions, pos):
    
    # Returns the corresponding region-index of a given position (CA_coordinates index)
    
    index_not_found = -1
    
    for index, reg in enumerate(regions):
        start, end = reg
        if pos <= end:
            if pos >= start:
                return index
        else:
            continue
            
    return index_not_found

def build_path(paths, regions, number_of_paths = 4):

    # If there is not one coherent path around the barrel this functions builds together different paths 
    # to get one path around the barrel with intermediate jumps.
    # The number of different paths that should be build if there is no coherent path can be changed
    
    new_path = []
    all_new_path = []
    longest_paths =[]
    start_regions =[]
    end_regions =[]
    decoy_paths = paths.copy()
    found_one_path = False
    
    for i in range(number_of_paths):
        lo_path, st_reg, en_reg = find_longest_path(decoy_paths, regions)
        
        if st_reg == en_reg:
            found_one_path = True
            break
        
        longest_paths.append(lo_path)
        start_regions.append(st_reg)
        end_regions.append(en_reg)
        decoy_paths.remove(lo_path)
    
    if not found_one_path:
        sorted_paths = sort_paths_to_regions(paths, regions)        
        for index, path in enumerate(longest_paths):
            new_path = glue_path(sorted_paths, end_regions[index], start_regions[index], regions, path)
            if not new_path:
                new_path = []
            else: 
                all_new_path.append(new_path)
                new_path = []
                
    else:
        all_new_path.append(lo_path)

    if not all_new_path:
        print('Was not able to build any path')
        return None
        
    return all_new_path

def find_longest_path(paths, regions):

    # Functions gives back the longest path in the list of all paths resived, as well as 
    # the starting and ending regions of this path
    
    all_lengths = []
    
    for path in paths:
        all_lengths.append(len(path))
    
    index_longest_path = all_lengths.index(max(all_lengths))

    longest_path = paths[index_longest_path]
    
    start = longest_path[0]
    end = longest_path[-1]
    start_region = search_region(regions, start[0])
    end_region = search_region(regions, end[1])
    
    return longest_path, start_region, end_region

def sort_paths_to_regions(paths, regions):

    # Sorts the paths according to the starting region
    
    regiontracker = 0
    sorted_path = [] 
    region_path = []
    start, end = regions[regiontracker]
    
    for index, path in enumerate(paths):
        first_con = path[0]
        
        if first_con[0] <= end:
            region_path.append(path)
        else:
            regiontracker += 1
            start, end = regions[regiontracker]
            sorted_path.append(region_path)
            region_path = []
            region_path.append(path)
            
    sorted_path.append(region_path)
    
    return sorted_path

def glue_path(sorted_paths, curr_region, stop_region, regions, new_path):

    # Used function by build_path to glue togehter paths until the start is reached again. 
    # Allways taking the longest path to continue 
    
    if curr_region == stop_region:
        return new_path
    
    paths_of_curr_region = sorted_paths[curr_region]
    try:
        path, start_region, end_region = find_longest_path(paths_of_curr_region, regions)
    except:
        print('No paths found')
        return None

    if (len(path)+len(new_path)) > len(regions):
        diverence = (len(path) + len(new_path)) - len(regions)
        for i in range(diverence):
            del path[-1]
    
    glued_path = new_path + path
    try:
        end = path [-1]
        new_curr_region = search_region(regions, end[1])
        next_path = glue_path(sorted_paths, new_curr_region, stop_region, regions, glued_path)
    except:
        print('was not able to build path further')
        return None
    
    return next_path

## 9.3 Assign directions, check the paths, calculate all the shears and find the minimum shear

def sheet_directions(regions, CA_coordinates, threshold = 45):

    # Asign each sheet a direction (1 or -1) starting in the first region (origin) with 1.
    # 1 for parallel to origin and -1 for antiparallel
    # The direction is calculated by calculating the angle between the two Calpha vector 
    # The threshold represents the angle for the decision between the directions ( <= is parallel / > antiparallel)
    # Returns a list of directions corresonding to the regions
    
    directions = []

    for index, curr_region in enumerate(regions):
        if index == 0:
            directions.append(1)
            continue

        prev_region = regions[index-1]
        start_reg = curr_region[0]
        end_reg = curr_region[1]
        start_prev = prev_region[0]
        end_prev = prev_region[1]

        if (end_reg - start_reg) < 5 or (end_prev - start_prev) < 5:
            curr_C_vector = array(CA_coordinates[start_reg + 2]) - array(CA_coordinates[start_reg])
            prev_C_vector = array(CA_coordinates[start_prev + 2]) - array(CA_coordinates[start_prev])
            vector_angle = angle(prev_C_vector, curr_C_vector, in_degrees = True)
        else:
            curr_C_vector = array(CA_coordinates[curr_region[0] + 4]) - array(CA_coordinates[curr_region[0] + 2])
            prev_C_vector = array(CA_coordinates[prev_region[0] + 4]) - array(CA_coordinates[prev_region[0] + 2])
            vector_angle = angle(prev_C_vector, curr_C_vector, in_degrees = True)

        if vector_angle <= threshold:
            if directions[-1] == 1:
                directions.append(1)
            else: 
                directions.append(-1)
        else:
            if directions[-1] == 1:
                directions.append(-1)
            else:
                directions.append(1)
    return directions

def check_paths(paths, regions):

    # checks if the path is vaild before calculating the shear (last sanity check)
    # checks if the path passes every region in the correct order and does not make any jumps

    selected_paths = paths.copy()
    regionpaths = draw_paths(paths, regions, showStart = False, returnstring = True)
    for index, regionpath in enumerate(regionpaths):
        if regionpath[0] == regionpath[-1]:
            for reg_index, region in enumerate(regionpath):
                if reg_index < len(regionpath)-1:
                    next_region = regionpath[reg_index+1]
                    differnces = abs(next_region-region)
                    if differnces > 1 and (next_region != 0 and region != len(regions)-1):
                        try:
                            selected_paths.remove(paths[index])
                        except:
                            pass
        else:
            selected_paths.remove(paths[index]) 
    if not selected_paths:
        return None
    else: 
        return selected_paths

def calculate_shear(glued_paths, directions, regions):

    # Calculates the shear number from all the paths it resives by taking into account the direction of the region

    all_shears = []
    each_change = []
    for path in glued_paths:
        for index, con in enumerate(path):
            if index < len(path)-1:
                next_con = path[index+1]
            else:
                next_con = path[0]
            
            jump = con[1]-next_con[0]
            reg_index = search_region(regions, con[1])
            sign = directions[reg_index]
            updated_jump = jump*sign
            
            each_change.append(updated_jump)            
            
        all_shears.append(sum(each_change))
        each_change = []
    return all_shears

def find_min_shear(shears):
    
    # Returns the minimum of all shears calculated
    
    try : 
        shear = min(shears)
        return shear
    except:
        print('No shear found')
        return None

## 9.4 Paths conversion to residue numbers, draw paths and build pymol command for path selection
    
def conversion_to_res_num(path, res_nums):

    # Converts the CA_coordinates indexes of the path to residue numbers    
    
    new_path = []
    
    for edge in path:
        a = res_nums[edge[0]]
        b = res_nums[edge[1]]
        new_path.append((a, b))
        
    return new_path

def draw_paths(paths, regions, showStart = False, returnstring = False, resinum = False):

    # Function to draw out the paths with the possibilty to show the start of the connections

    arrow = ' -> '
    path = ''
    regionpath = ''
    all_regionpaths = []
    for k, p in enumerate(paths):
        path = 'Path '+ str(k) + ': '
        for j, con in enumerate(p):
            if j == 0:
                start = str(con[0])
                end = str(con[1])
                path = path + start + arrow + end
                start_reg = search_region(regions, con[0])
                end_reg = search_region(regions, con[1])
                regionpath = str(start_reg) + arrow + str(end_reg)
            else:
                start = str(con[0])
                end = str(con[1])
                if showStart:
                    path = path + arrow + '('+ start + ')' + ' '+ end
                else:
                    path = path + arrow + ' '+ end
                    end_reg = search_region(regions, con[1])
                    regionpath = regionpath + arrow + str(end_reg)
        all_regionpaths.append([int(x) for x in regionpath.split(arrow)])
        # if not returnstring:
        #     print(path)
        #     if not resinum:
        #         print('Regionpath')
        #         print(regionpath)
        start = ''
        end = ''
        path = ''
        regionpath = ''
    if returnstring:
        return all_regionpaths

def draw_for_pymol(paths, showStart = False):

    # Prints out a command line for pymol to select all residues of the path

    plus = ' + '
    path = ''
    select = 'select '
    resi = 'resi '
    for k, p in enumerate(paths, start = 1):
        path = 'Command for path '+ str(k) + ':  '
        for j, con in enumerate(p):
            if j == 0:
                start = str(con[0])
                end = str(con[1])
                path = path + select + resi + start + plus + resi + end
            else:
                start = str(con[0])
                if start != end:
                    path = path + plus + resi + start
                end = str(con[1])
                path = path + plus + resi + end
                    
        # print(path)
        start = ''
        end = ''
        path = ''

# 10. Functions to estimate a and b distances (acccording to Murzin 1994)

def calculate_a(pdb_file):

    CA_coordinates, res_nums = get_CA_coordinates(pdb_file)
    regions = find_regions(res_nums)

    a = []
    for region in regions:
        for i in range(region[0], region[1]-1):
            distance = 0
            coordA = CA_coordinates[i]
            coordB = CA_coordinates[i+2]

            for j in range(len(coordA)):
                distance += (coordA[j] - coordB[j])*(coordA[j] - coordB[j])
            curr_a = sqrt(distance)/2
            a.append(curr_a)

    return median(a), mad(a)

def calculate_b(pdb_file):

    CA_coordinates, res_nums = get_CA_coordinates(pdb_file)
    regions = find_regions(res_nums)

    b = compute_contacts_between_regular_regions(regions, CA_coordinates, mode = 'median')
    b = [min(i) for i in b if not isnan(min(i))]

    return median(b), mad(b)

# 11. Functions to calculate Ca-Ca distances (c) and bond angles

def calculate_c(pdb_file):

    CA_coordinates, res_nums = get_CA_coordinates(pdb_file)
    regions = find_regions(res_nums)

    a = []
    for region in regions:
        for i in range(region[0], region[1]):
            distance = 0
            coordA = CA_coordinates[i]
            coordB = CA_coordinates[i+1]

            for j in range(len(coordA)):
                distance += (coordA[j] - coordB[j])*(coordA[j] - coordB[j])
            curr_a = sqrt(distance)
            a.append(curr_a)

    return median(a), mad(a)

def calculate_CA_pseudoangles(pdb_file):

    CA_coordinates, res_nums = get_CA_coordinates(pdb_file)
    regions = find_regions(res_nums)

    theta1 = []
    kappa  = []

    for region in regions:
        for i in range(region[0]+1, region[1]-1):
            
            curr_vector = array(CA_coordinates[i+1]) - array(CA_coordinates[i])
            next_vector = array(CA_coordinates[i+2]) - array(CA_coordinates[i])  
            kappa.append(angle(curr_vector, next_vector, in_degrees = True))

            curr_vector = array(CA_coordinates[i+1]) - array(CA_coordinates[i])
            next_vector = array(CA_coordinates[i-1]) - array(CA_coordinates[i])            
            theta1.append(angle(curr_vector, next_vector, in_degrees = True))

    return median(theta1), mad(theta1), median(kappa), mad(kappa)


#### FUNCTIONS TO PLOT OVERALL DATA

def plot_parameter(x_col, y_col, df, saveto, fit_line = False):

    x = df[x_col]
    y = df[y_col]
    plt.clf()
    
    if '{}_mad'.format(y_col) in df.columns:
        plt.errorbar(x, y, yerr = df['{}_mad'.format(y_col)], linestyle="None", fmt = '.', color = 'black')
        plt.xlabel('Number of strands')
        plt.ylabel(r'Approximated {}'.format(y_col))
        plt.title('Approximate beta barrel {} for {} beta-barrel chains'.format(y_col, len(x)))

    else:
        plt.scatter(x, y, color = 'black', marker = '.')
        plt.xlabel('Number of strands')
        plt.ylabel(r'Approximated {}'.format(y_col))
        plt.title('Approximate beta barrel {} for {} beta-barrel chains'.format(y_col, len(x)))

    if fit_line:

        # add linear regression data and abline
        slope, intercept, r_value, p_value, std_err = stats.linregress(x,y)
        fit_x = range(min(x), max(x)+1)
        fit_y = [slope * a + intercept for a in fit_x]
        if intercept == 0:
            label = r'$y = {:.2f}x  (R^2 = {:.2f}$)'.format(slope, r_value*r_value)
        elif intercept < 0:
            label = r'$y = {:.2f}x - {:.2f}  (R^2 = {:.2f}$)'.format(slope, intercept*(-1), r_value*r_value)
        elif intercept > 0:
            label = r'$y = {:.2f}x + {:.2f}  (R^2 = {:.2f}$)'.format(slope, intercept, r_value*r_value)
        else:
            label = 'no fit'

        plt.plot(fit_x, fit_y, '--', color = 'black', label = label)
        plt.legend()

    plt.savefig(saveto)


#### THE MAIN BARROS METHOD ####

def run_barros(arguments, offset = 1, step = 2, local_angle_threshold = 25, max_loop_size = 0):

    job_number, input_mode, complexes_only, multimer_only, delete, distance_threshold, extract_hairpins, in_queue = arguments

    # create output files to save the sequences
    outfasta = "full_sequences_matched_pdbs_job{}.fasta".format(job_number)
    outpt = open(outfasta, 'w')

    outbarrels = "barrel_sequences_matched_pdbs_job{}.fasta".format(job_number)
    outbb = open(outbarrels, 'w')

    outmultimeric = "multimeric_barrel_sequences_matched_pdbs_job{}.fasta".format(job_number)
    outmt = open(outmultimeric, 'w')

    outdata = "barrels_matched_pdbs_data_job{}.pickle".format(job_number)

    if os.path.isfile(outdata):
        data = pickle.load(open(outdata, 'rb'))

    else:
        data = {'PDBs': [],
                'TMsegm': [],
                'BB_diameter': [],
                'BB_diameter_mad': [],
                'Shear': [],
                'Membrane_thickness': [],
                # 'a': [],
                # 'a_mad': [],
                # 'b': [],
                # 'b_mad': [],
                # 'h': [],
                # 'CAi-CAi+1': [],
                # 'CAi-CAi+1_mad': [],
                'CAi-CAi+1-CAi+2': [],
                'CAi-CAi+1-CAi+2_mad': [],
                'CAi+2-CAi-CAi+1': [],
                'CAi+2-CAi-CAi+1_mad': [],
                'N_residues': [],
                'N_chains': [],
                'Multimeric': []}
    
    for i, pdbID in enumerate(in_queue):
        deleted_it = False
        isfile = False

        print(" ... Taking care of '{}' (job {}) ({}/{})".format(pdbID, job_number+1, i+1, len(in_queue)))

        if '.pdb' in pdbID or '.ent' in pdbID:

            isfile = True
            pdb_file = pdbID
            chains_inpdb = get_chains_in_pdb(pdb_file, source_pdb=False)[0]

            if len(chains_inpdb) > 0:
                try:
                    pdbID, chainID = pdb_file[:-4].split('/')[-1].split('_')
                except:
                    pdbID, chainID = pdb_file[:-4], chains_inpdb[0]
                if len(pdbID) > 4:
                    pdbID, chainID = pdb_file[:-4], chains_inpdb[0]
            else:
                pdb_file = 'not available'

            # check if this is a OPM structure
            membrane_thickness = parse_membrane_thickness_from_OMP(pdb_file)
            if membrane_thickness > 0:
                protein_type = 'membrane'
            else:
                protein_type = 'non-membrane'
                membrane_thickness = None

        else:    
            pdb_file, protein_type, membrane_thickness = download_pdb(pdbID)
            pdbID, chainID = pdbID.split('_')

        if complexes_only and pdb_file != 'not available':
            chains_inpdb, pdb_file = get_chains_in_pdb(pdb_file)
            target_chains = []
            if len(chains_inpdb) > 1:
                for chainID in chains_inpdb:
                    if isfile:
                        chain_pdb, _ = extract_chain(pdb_file, chainID, outpdb = '{}_{}.pdb'.format(pdbID, chainID))
                        target_chains.append([pdbID.split('.')[0], chainID, chain_pdb, protein_type, membrane_thickness])
                    else:  
                        chain_pdb, protein_type, membrane_thickness = download_pdb('{}_{}'.format(pdbID.split('_')[0], chainID))
                        target_chains.append([pdbID.split('_')[0], chainID, chain_pdb, protein_type, membrane_thickness])
            else:
                target_chains = None
        elif pdb_file != 'not available':
            if 'AF-' not in pdbID and 'MGY' not in pdbID:
                target_chains = [[pdbID.split('_')[0], chainID, pdb_file, protein_type, membrane_thickness]]
            else:
                try:
                    target_chains = [[pdbID.split('/')[1], chainID, pdb_file, protein_type, membrane_thickness]]
                except:
                    target_chains = [[pdbID, chainID, pdb_file, protein_type, membrane_thickness]]

        if pdb_file != 'not available' and protein_type != 'not available' and target_chains is not None:

            if input_mode != 'all':
                if protein_type != input_mode and delete:
                    os.system("rm {}".format(pdb_file))
                    #print(" ... ... pdbID '{}' is '{}' (different from '{}'). Will delete it!".format(pdbID, protein_type, input_mode))
                    deleted_it = True

            if not deleted_it:
                if protein_type == 'membrane':
                    print(" ... ... Protein {} is inserted into a membrane with {} Ang thickness".format(pdbID, membrane_thickness))
                #else:
                    #print(" ... ... Protein {} is NOT inserted into a membrane. Membrane thickness set to '{}'".format(pdbID, membrane_thickness))

                for pdbID, chainID, pdb_file, protein_type, membrane_thickness in target_chains:

                    if '{}_{}'.format(pdbID, chainID) not in data['PDBs']:

                        try:
                            pdb_sequence, barrel_topology, chains, num_bb_regions, pdb_file = process_barrel(pdb_file, ID = pdbID, chainID = chainID, offset = offset, step = step, local_angle_threshold = local_angle_threshold, distance_threshold = distance_threshold, multimer_only=multimer_only)
                        except:
                            num_bb_regions = 0

                        # check if it has a barrel. If so, continue
                        if num_bb_regions > 0 and len(pdb_sequence) < 10000:
                            # get the sequence of the barrel region only
                            barrel_seq, barrel_struct = remove_long_loops_from_barrel(pdb_sequence, pdb_file, barrel_topology, max_loop_size = 10)

                            print('\n\t>{}TM_BARREL_{}_{}_{}'.format(num_bb_regions, protein_type, pdbID, chainID))
                            print('\t',pdb_sequence)
                            print('\t',barrel_topology,'\n')

                            outpt.write('>{}TM_BARREL_{}_{}_{}\n'.format(num_bb_regions, protein_type, pdbID, chainID))
                            outpt.write('{}\n'.format(pdb_sequence))
                            outpt.write('{}\n'.format(barrel_topology))

                            # if it is a membrane protein, check whether the barrel is inserted into the membrane. Otherwise consider it does not
                            if protein_type == 'membrane':
                                barrel_crosses_membrane, barrel_struct = find_if_barrel_crosses_membrane(pdb_sequence, pdb_file, barrel_topology, membrane_thickness, max_loop_size = max_loop_size)
                            else:
                                barrel_crosses_membrane = False
                            print(" ... ... Barrel in {} crosses membrane: {}".format(pdbID, barrel_crosses_membrane))

                            if protein_type != 'membrane' or (protein_type == 'membrane' and barrel_crosses_membrane):
                                # if the barrel does not cross the membrane, move and rotate it so that it is in a reference position (centered in (0,0,0) with the axis of the pore in z)
                                #if not barrel_crosses_membrane:
                                print(" ... ... Trying to move the barrel to the reference position (centered in (0,0,0) with the axis of the pore in z)")
                                pdb_file, curr_angle = move_barrel_to_reference_position(pdb_file, barrel_topology, center = [0,0,0])

                                # calculate the diameter of the barrel ONLY (no loops at all)
                                print(" ... ... Computing barrel diameter (excluding all loops)")
                                barrel_seq, barrel_struct = remove_long_loops_from_barrel(pdb_sequence, pdb_file, barrel_topology, max_loop_size = 0)
                                diameter, mad_diameter = get_barrel_diameter(barrel_struct, chainID, min_residue_numb = int(num_bb_regions/5.0), step_z = 1, membrane_thickness = membrane_thickness)
                                
                                if diameter is not None:
                                    print(" ... ... ... Barrel diameter: {} (+/- {}) Ang".format(round(diameter,2), round(mad_diameter,2)))

                                # calculate the shear number
                                print(" ... ... Computing the shear number")
                                try:
                                    shear = shear_number(barrel_struct, show_path = False)
                                    if shear is not None:
                                        print(' ... ... ... Shear: {}'.format(shear))
                                    else:
                                        print(' ... ... ... Error: Not possible to compute the shear number')
                                except:
                                    shear = None
                                    print(' ... ... ... Error: Not possible to compute the shear number')

                                # calculate a (the distance between two adjacent CA assuming they are in the same line defined by the strand)
                                # print(' ... ... Computing a and b')
                                # a, mad_a = calculate_a(barrel_struct)
                                # print(' ... ... ... a: {} (+/- {}) Ang'.format(round(a, 2), round(mad_a, 2)))

                                # b, mad_b = calculate_b(barrel_struct)
                                # print(' ... ... ... b: {} (+/- {}) Ang'.format(round(b, 2), round(mad_b, 2)))

                                # calculate the median distance between two adjacent c-alphas
                                # print(' ... ... Computing local CA geometric features (distance to next CA and and the bond angles to CAi-1 and CAi+2)')
                                # c, mad_c = calculate_c(barrel_struct)
                                # print(' ... ... ... CAi-CAi+1 distance: {} (+/- {}) Ang'.format(round(c, 2), round(mad_c, 2)))
                                theta1, theta1_mad, kappa, kappa_mad = calculate_CA_pseudoangles(barrel_struct)
                                print(' ... ... ... CAi-CAi+1-CAi+2 angle: {} (+/- {}) degrees'.format(round(theta1, 2), round(theta1_mad, 2)))
                                print(' ... ... ... CAi+2-CAi-CAi+1 angle: {} (+/- {}) degrees'.format(round(kappa, 2), round(kappa_mad, 2)))
                                # h = sqrt(c**2-a**2)
                                # print(' ... ... ... h: {} Ang'.format(round(h, 2)))

                                # save all results into the dictionary
                                data['PDBs'].append('{}_{}'.format(pdbID, chainID))
                                data['TMsegm'].append(num_bb_regions)
                                data['BB_diameter'].append(diameter)
                                data['BB_diameter_mad'].append(mad_diameter)
                                data['Shear'].append(shear)
                                data['Membrane_thickness'].append(membrane_thickness)
                                # data['a'].append(a)
                                # data['a_mad'].append(mad_a)
                                # data['b'].append(b)
                                # data['b_mad'].append(mad_b)
                                # data['CAi-CAi+1'].append(c)
                                # data['CAi-CAi+1_mad'].append(mad_c)
                                data['CAi-CAi+1-CAi+2'].append(theta1)
                                data['CAi-CAi+1-CAi+2_mad'].append(theta1_mad)
                                data['CAi+2-CAi-CAi+1'].append(kappa)
                                data['CAi+2-CAi-CAi+1_mad'].append(kappa_mad)
                                # data['h'].append(h)
                                data['N_residues'].append(len(pdb_sequence))

                                # extract the sequence of the barrel domain only
                                print(" ... Getting final barrel structure without N- and C-termini")
                                print(' ... ... Masking out internal loops from the topology string')
                                masked_topology = mask_loops(barrel_topology, chains)
                                barrel_seq, barrel_struct = remove_long_loops_from_barrel(pdb_sequence, pdb_file, masked_topology, max_loop_size = 10, chains=chains)
                                print(" ... ... Saved as: {}".format(barrel_struct))                    

                                # check if the barrel is multimeric and save the sequences accordingly
                                chains_in_barrel = get_chains_in_pdb(barrel_struct, source_pdb=False)[0]
                                n_chains = len(chains_in_barrel)                    
                                data['N_chains'].append(n_chains)
                                data['Multimeric'].append(n_chains > 1)

                                if n_chains > 1:
                                    chains_seqs = get_chains_sequence(pdb_sequence,chains)
                                    for chain in chains_in_barrel:
                                        outmt.write('>{}TM_BARREL_{}_{}_{}\n'.format(num_bb_regions, protein_type, pdbID, chain))
                                        outmt.write('{}\n'.format(chains_seqs[chain]))
                                
                                else:
                                    outbb.write('>{}TM_BARREL_{}_{}_{}\n'.format(num_bb_regions, protein_type, pdbID, chainID))
                                    outbb.write('{}\n'.format(barrel_seq))
                            else:
                                print('... ... There is a barrel in {}_{} but it does not cross the membrane\n'.format(pdbID, chainID))
                                if delete:
                                    os.system("rm {}*".format(pdb_file[:-4]))

                            if extract_hairpins and os.path.isfile(barrel_struct):
                                save_hairpins(pdb_sequence, pdb_file, barrel_topology, chains=chains, outfolder='hairpins')

                            # save intermediate collected data
                            pickle.dump(data, open(outdata, 'wb'))


                        else:
                            print('... ... Not able to detect barrel topology for {}_{}\n'.format(pdbID, chainID))
                            if delete:
                                os.system("rm {}*".format(pdb_file[:-4]))

                            else:
                                seqstruct, pdb_sequence, chains = get_secondary_structure(pdb_file)
                                
                                outbb.write('>NaN_TM_BARREL_{}_{}_{}\n'.format(protein_type, pdbID, chainID))
                                outbb.write('{}\n'.format(pdb_sequence))

                                if extract_hairpins:
                                    save_hairpins(pdb_sequence, pdb_file, seqstruct, marker='E', chains=chains, outfolder='hairpins')


        else:
            print(" ... ... pdbID '{}_{}' impossible to get".format(pdbID, chainID))
    
    outpt.close()
    outbb.close()
    outmt.close()

    return pd.DataFrame(data)
        
