import sys
import random
import multiprocessing as mp
import barrOs_library as barros

number_of_jobs = 1

# Greet
barros.print_hello()

# Get inputs
input_files, input_types, input_mode, outf, complexes_only, multimer_only, delete, distance_threshold, extract_hairpins = barros.get_inputs(sys.argv)

# Check if inputs are correct in case we are not dealing with a 'help' check

if __name__ ==  '__main__':
    if input_mode != 'help':
        
        input_status = barros.input_check(input_files, input_mode)

        # Proceed only if the input_status is 'all good', which means there are input strings and a valid mode
        if input_status != 'all good':
            barros.print_help()
            exit
        else:
            # Print the summary of the input parameters
            barros.print_summary(input_files, input_types, input_mode)

            # Get all pdbid from the set of inputs
            pdbIDs = barros.get_all_pdbIDs(input_files, input_types)
            if number_of_jobs == 1:
                pdbIDs = barros.sort(pdbIDs)
            else:
                barros.shuffle(pdbIDs)
            print(" ... Note: Collected a total of {} chains as putative chains to analyse".format(len(pdbIDs)))

            # Prepare all parallel jobs and run the main barrOs method for each pdbid
            separated_jobs = barros.chunk_list(pdbIDs, number_of_jobs)
            list_arguments = [i for i in zip(range(number_of_jobs), [input_mode for job in separated_jobs],[complexes_only for job in separated_jobs],[multimer_only for job in separated_jobs],[delete for job in separated_jobs],[distance_threshold for job in separated_jobs],[extract_hairpins for job in separated_jobs], separated_jobs)]

            pool = mp.Pool(number_of_jobs)
            data = pool.map(barros.run_barros, list_arguments)
            data = barros.pd.concat(data).reset_index(drop=True)
            print()
            print(data)

            pool.close()
            pool.join()

            data.to_csv(outf, sep='\t')

            if len(pdbIDs) > 1:
                for param in data.columns:
                    if 'mad' not in param and param != 'PDBs' and param != 'TMsegm':
                        try:
                            barros.plot_parameter('TMsegm', param, data.loc[data.N_chains == 1], fit_line = True, saveto = '{}_{}_monomers.pdf'.format(outf.split('.')[-2], param))
                            barros.plot_parameter('TMsegm', param, data.loc[data.N_chains > 1], fit_line = True, saveto = '{}_{}_multimers.pdf'.format(outf.split('.')[-2], param))
                        except:
                            print('ERROR: Not enough values to plot for', param)

            
        

            
                
            
