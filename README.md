# barrOs

## Dependencies

barrOs is written in Python 3.7. It requires mostly core Python modules and only three external packages are required: 
  - Seaborn
  - Pandas
  - Networkx 

It also requires DSSP (https://swift.cmbi.umcn.nl/gv/dssp/) to be installed.

## Installation

### Installing from Source

Download the zip archive or clone the repository with git:

```
# To download
git clone https://git.scicore.unibas.ch/schwede/barrOs
cd barrOs

# To update
git pull origin master
```

## Allowed inputs
  
## Usage
```
Usage: python3 barrOs.py -in:<type>:<input_file> -out:<mode>

Parameters:
     -in:<type>  	an input string (or list of strings) with one of the following types: ['pdb_file', 'pdbID', 'hhpred', 'pdbsum_fasta']
                 	for example: -in:pdbID:1LML_A,3XML_A or -in:pdb_file:1LML_A.pdb,3XML_A.pdb
                 	ATENTION: barrOs only works with monomeric proteins, so always provide the target chain
     -mode:      	defines the mode of running, i.e. either we want to deal only with membrane proteins, all, or non-membrane proteins
                 	values accepted: ['membrane', 'non-membrane', 'all']

```
### Examples
barrOs can take different input types (i.e., PDB files, PDB IDs including chain ID, HHpred result files, of fasta files with PDB IDs in the headers), as well as different modes that allow the focus on membrane proteins if necessary:
```
# For a single pdbID
python3 barrOs.py -in:pdbID:1BXW_A -mode:membrane


# For a list of hhr files
python3 barrOs.py -in:hhpred:examples/hhpred_3686809.hhr,examples/hhpred_3686809_10.hhr,examples/hhpred_3686809_12.hhr,examples/hhpred_3686809_2.hhr,examples/hhpred_3686809_4.hhr,examples/hhpred_3686809_6.hhr,examples/hhpred_3686809_8.hhr,examples/hhpred_3686809_1.hhr,examples/hhpred_3686809_11.hhr,examples/hhpred_3686809_13.hhr,examples/hhpred_3686809_3.hhr,examples/hhpred_3686809_5.hhr,examples/hhpred_3686809_7.hhr,examples/hhpred_3686809_9.hhr -mode:membrane
```

## Acknowledgements

barrOs started in 2018 while Joana Pereira was a postdoc with Andrei Lupas at the Dept. of Protein Evolution of the Max Planck Institute for Developmental Biology. Its further development is now being carried out at Trosten Schwede's group at the Biozentrum of the University of Basel.
Special thanks to Jens Bassler and Andrei Lupas for helpful discussions and brainstormings when barrOs was being born, and to Roger Bamert for contributing to the implementation of the shear number calculation into the pipeline.
